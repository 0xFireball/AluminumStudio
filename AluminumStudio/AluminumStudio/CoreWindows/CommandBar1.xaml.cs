﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace AluminumStudio.CoreWindows
{
    /// <summary>
    /// Interaction logic for CommandBar1.xaml
    /// </summary>
    public partial class CommandBar1
    {
        public CommandBar1()
        {
            InitializeComponent();
        }

        private void MetroWindow_Loaded(object sender, RoutedEventArgs e)
        {
            commandBox.Focus();
        }

        private void commandBox_LostFocus(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
