﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using Newtonsoft.Json;
using System.IO;
using AluminumStudio.StudioInternal;
using AluminumStudio.StudioInternal.ProjectSupport;
using System.Xml;
using MahApps.Metro.Controls.Dialogs;
using System.Threading;

namespace AluminumStudio.CoreWindows
{
    /// <summary>
    /// Interaction logic for IntegratedEditor1.xaml
    /// </summary>
    public partial class IntegratedEditor1
    {
        string _projectFile;
        string _projectDir;
        string _currentFile = null;
        static char dirSep = Path.DirectorySeparatorChar;
        AluminumProject _currentProject;
        IAluminumProjectLauncher _launcher;
        bool customLauncher = false;
        int lastSelectedIndex = 0;

        public IntegratedEditor1(string projectFile)
        {
            _projectFile = projectFile;
            InitializeComponent();
            OutputWriteLine("Loading editor...");
        }

        private async void MetroWindow_Loaded(object sender, RoutedEventArgs e)
        {
            if (!(await LoadProjectItems()))
                _currentProject.ProjectItems = new List<string>();
            PrepareEditor();
            OutputWriteLine("Editor loaded.");
        }

        private void DetectProjectLauncher()
        {
            //detect
            switch (_currentProject.ProjectType)
            {
                case AluminumProjectType.ProcessingWeb:
                    _launcher = new ProcessingWebLauncher();
                    break;
                default:
                    string customLaunchFile = _projectDir + "launch.json";
                    customLauncher = false;
                    if (File.Exists(customLaunchFile))
                    {
                        try
                        {
                            string launchJson = File.ReadAllText(customLaunchFile);
                            _launcher = JsonConvert.DeserializeObject<CustomLauncher>(launchJson);
                            //File.WriteAllText(customLaunchFile, JsonConvert.SerializeObject(_launcher, Newtonsoft.Json.Formatting.Indented));
                            if (_launcher == null)
                                throw new JsonSerializationException("The file launch.json contains invalid JSON data. Error: Value cannot be null.");
                            customLauncher = true;
                        }
                        catch (Exception ex)
                        {
                            new ErrorDisplay(ex.ToString(), CrashType.ActionCrash).ShowDialog();
                        }
                    }
                    break;
            }

            //prepare buttons accordingly
            if (_launcher != null)
            {

            }
        }

        private void PrepareEditor()
        {
            /*
            var inputHandler = new ICSharpCode.AvalonEdit.Editing.TextAreaInputHandler(textEditor.TextArea);
            textEditor.TextArea.ActiveInputHandler=inputHandler;
            var saveCommand = new RoutedCommand();
            inputHandler.AddBinding(saveCommand, ModifierKeys.Control, Key.S, OnSave);
            */
            //textEditor.TextArea.DefaultInputHandler.NestedInputHandlers.Add(new ICSharpCode.AvalonEdit.Search.SearchInputHandler(textEditor.TextArea));
            //textEditor.TextArea.DefaultInputHandler.NestedInputHandlers.Add(ICSharpCode.AvalonEdit.Search.SearchPanel.Install(texte));
            ICSharpCode.AvalonEdit.Search.SearchPanel.Install(textEditor);

        }

        private void OnSave(object sender, ExecutedRoutedEventArgs e)
        {
            textEditor.Save(_currentFile);
        }

        private async Task<bool> LoadProjectItems()
        {
            fileList.Items.Clear();
            string pName = Path.GetFileNameWithoutExtension(_projectFile);
            _projectDir = Path.GetDirectoryName(_projectFile) + dirSep;
            _currentProject = JsonConvert.DeserializeObject<AluminumProject>(File.ReadAllText(_projectFile));
            _currentProject.Name = pName;
            fileList.Items.Add(new ProjectItem(_projectFile));
            if (_currentProject.ProjectItems == null)
            {
                await this.ShowMessageAsync("Error", "The project file seems to be invalid. Verify that the ProjectItems element exists.");
                return false;
            }
            foreach (var item in _currentProject.ProjectItems)
            {
                if (Path.IsPathRooted(item))
                {
                    var projItem = new ProjectItem(item);
                    fileList.Items.Add(projItem);
                }
                else
                {
                    var projItem = new ProjectItem(Path.Combine(_projectDir, item));
                    fileList.Items.Add(projItem);
                }
            }
            fileList.SelectedIndex = 0;
            DetectProjectLauncher();
            return true;
        }

        private async void fileList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            textEditor.IsEnabled = fileList.SelectedItem != null;
            if (fileList.SelectedItem != null)
            {
                if (_currentFile != null)
                    textEditor.Save(_currentFile);
                var currentFile = (ProjectItem)fileList.SelectedItem;
                _currentFile = currentFile.FullPath;
                if (!File.Exists(_currentFile))
                    File.WriteAllText(_currentFile, "");
                textEditor.Load(_currentFile);
                if (_currentFile == _projectFile)
                {
                    textEditor.SyntaxHighlighting = ICSharpCode.AvalonEdit.Highlighting.HighlightingManager.Instance.GetDefinition("JavaScript");
                }
                else
                {
                    switch (_currentProject.ProjectType)
                    {
                        case AluminumProjectType.ProcessingWeb:
                            textEditor.SyntaxHighlighting = LangSelection.GetHighlightingDefinition("Processing");
                            break;
                        default:
                            textEditor.SyntaxHighlighting = null;
                            string ext = Path.GetExtension(currentFile.FullPath);
                            var extDef = ICSharpCode.AvalonEdit.Highlighting.HighlightingManager.Instance.GetDefinitionByExtension(ext);
                            if (extDef == null)
                            {
                                extDef = LangSelection.GetHighlightingDefinition(ext);
                                if (extDef == null)
                                    textEditor.SyntaxHighlighting = ICSharpCode.AvalonEdit.Highlighting.HighlightingManager.Instance.GetDefinition("MarkDown");
                            }
                            if (textEditor.SyntaxHighlighting == null)
                            {
                                textEditor.SyntaxHighlighting = extDef;
                            }
                            break;
                    }
                }
            }
        }

        private async void textEditor_KeyDown(object sender, KeyEventArgs e)
        {
            if (Keyboard.IsKeyDown(Key.LeftCtrl) || Keyboard.IsKeyDown(Key.RightCtrl))
            {
                if (e.Key == Key.S)
                {
                    OnSave(null, null);
                }
                if (e.Key == Key.N)
                {
                    CreateNewFile();
                }
                if (e.Key == Key.Tab)
                {
                    int lastinx = lastSelectedIndex;
                    lastSelectedIndex = fileList.SelectedIndex;
                    fileList.SelectedIndex = lastinx;
                    e.Handled = true;
                }
                if (e.Key == Key.B)
                {
                    OutputWriteLine("Running Task 'Build'");
                    await StartSTATask<bool>(() => { BuildProject(); return true; });
                    OutputWriteLine("Build Completed.");
                }
            }
            if (e.Key == Key.F5)
            {
                OutputWriteLine("Running Task 'Launch'");
                await StartSTATask<bool>(() => { LaunchProject(); return true; });
                OutputWriteLine("Launch Finished");
            }
            if (e.Key == Key.F1)
            {
                commandBox.Focus();
            }
        }

        private async void BuildProject()
        {
            await Task.Run(() =>
            {
                this.Dispatcher.Invoke(async () =>
                {
                    switch (_currentProject.ProjectType)
                    {
                        case AluminumProjectType.ProcessingWeb:
                            List<string> piFullPaths = new List<string>();
                            foreach (object pi in fileList.Items)
                            {
                                piFullPaths.Add(((ProjectItem)pi).FullPath);
                            }
                            //Filter by PDE files, then return full text of sources
                            List<string> sourcesStr = piFullPaths.Where(fp => new FileInfo(fp).Extension == ".pde").ToList().Select(fp => File.ReadAllText(fp)).ToList();
                            DirectoryInfo outputDir = new DirectoryInfo(_projectDir + "Output");
                            if (!outputDir.Exists)
                                outputDir.Create();
                            if (_launcher.CanBuild)
                                _launcher.Build(string.Join("\n", sourcesStr), outputDir);
                            else
                            {
                                await this.ShowMessageAsync("Error", "'Build' is not a valid target for the current launch configuration.");
                            }
                            break;
                        default:
                            DetectProjectLauncher();
                            if (!customLauncher)
                            {
                                await this.ShowMessageAsync("Error", "There is no valid launch configuration defined. Create or validate a 'launch.json' to launch the project.");
                                break;
                            }
                            if (_launcher.CanBuild)
                                _launcher.Build(_projectDir);
                            else
                            {
                                await this.ShowMessageAsync("Error", "'Build' is not a valid target for the current launch configuration.");
                            }
                            break;
                    }
                });
            });
        }

        private void CreateNewFile()
        {

        }

        private async void newCtx_Click(object sender, RoutedEventArgs e)
        {
            var sfd = new Microsoft.Win32.SaveFileDialog();
            sfd.Title = "Save File";
            if ((bool)sfd.ShowDialog())
            {
                string newFileName = sfd.FileName;
                if (new FileInfo(newFileName).Name == "launch.json")
                    File.WriteAllText(newFileName, JsonConvert.SerializeObject(new CustomLauncher(), Newtonsoft.Json.Formatting.Indented));
                else
                    File.WriteAllText(newFileName, "");
                var projDirUri = new Uri(_projectDir);
                var nfnUri = new Uri(newFileName);
                newFileName = projDirUri.MakeRelativeUri(nfnUri).ToString();
                var newItem = new ProjectItem(_projectDir + sfd.FileName);
                _currentProject.ProjectItems.Add(newFileName);
                SaveProjectFile();
                await LoadProjectItems();
            }
        }

        private void SaveProjectFile()
        {
            File.WriteAllText(_projectFile, JsonConvert.SerializeObject(_currentProject, Newtonsoft.Json.Formatting.Indented));
        }

        private async void launchBtn_Click(object sender, RoutedEventArgs e)
        {
            await StartSTATask<bool>(() => { LaunchProject(); return true; });
        }

        private async void LaunchProject()
        {
            await Task.Run(() =>
            {
                this.Dispatcher.Invoke(async () =>
                {
                    switch (_currentProject.ProjectType)
                    {
                        case AluminumProjectType.ProcessingWeb:
                            List<string> piFullPaths = new List<string>();
                            foreach (object pi in fileList.Items)
                            {
                                piFullPaths.Add(((ProjectItem)pi).FullPath);
                            }
                            //Filter by PDE files, then return full text of sources
                            List<string> sourcesStr = piFullPaths.Where(fp => new FileInfo(fp).Extension == ".pde").ToList().Select(fp => File.ReadAllText(fp)).ToList();
                            DirectoryInfo outputDir = new DirectoryInfo(_projectDir + "Output");
                            if (!outputDir.Exists)
                                outputDir.Create();
                            if (_launcher.CanLaunch)
                                _launcher.Launch(string.Join("\n", sourcesStr), outputDir);
                            else
                            {
                                await this.ShowMessageAsync("Error", "'Launch' is not a valid target for the current launch configuration.");
                            }
                            break;
                        default:
                            DetectProjectLauncher();
                            if (!customLauncher)
                            {
                                await this.ShowMessageAsync("Error", "There is no valid launch configuration defined. Create or validate a 'launch.json' to launch the project.");
                                break;
                            }
                            if (_launcher.CanLaunch)
                                _launcher.Launch(_projectDir);
                            else
                            {
                                await this.ShowMessageAsync("Error", "'Launch' is not a valid target for the current launch configuration.");
                            }
                            break;
                    }
                });
            });
        }

        private void fileList_MouseEnter(object sender, MouseEventArgs e)
        {
            if (lastSelectedIndex != fileList.SelectedIndex)
                lastSelectedIndex = fileList.SelectedIndex;
        }

        private void commandBox_LostFocus(object sender, RoutedEventArgs e)
        {
            commandBox.Text = "";
        }

        public void SetLanguage(string langKeyword)
        {

        }

        private void commandBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            bool s = InterpretCommand(commandBox.Text);
            if (s)
                commandBox.Text = "";
        }

        private bool InterpretCommand(string text)
        {
            text = text.ToLower();
            switch (text)
            {
                case "lang haxe":
                    textEditor.SyntaxHighlighting = LangSelection.GetHighlightingDefinition("Haxe");
                    return true;
                case "lang java":
                    textEditor.SyntaxHighlighting = ICSharpCode.AvalonEdit.Highlighting.HighlightingManager.Instance.GetDefinition("Java");
                    return true;
                case "lang js":
                    textEditor.SyntaxHighlighting = ICSharpCode.AvalonEdit.Highlighting.HighlightingManager.Instance.GetDefinition("JavaScript");
                    return true;
                case "lang cpp":
                    textEditor.SyntaxHighlighting = ICSharpCode.AvalonEdit.Highlighting.HighlightingManager.Instance.GetDefinition("CPP");
                    return true;
                case "lang html":
                    textEditor.SyntaxHighlighting = ICSharpCode.AvalonEdit.Highlighting.HighlightingManager.Instance.GetDefinition("HTML");
                    return true;
                case "lang xml":
                    textEditor.SyntaxHighlighting = ICSharpCode.AvalonEdit.Highlighting.HighlightingManager.Instance.GetDefinition("XML");
                    return true;
                case "lang php":
                    textEditor.SyntaxHighlighting = ICSharpCode.AvalonEdit.Highlighting.HighlightingManager.Instance.GetDefinition("PHP");
                    return true;
                case "lang bat":
                    textEditor.SyntaxHighlighting = ICSharpCode.AvalonEdit.Highlighting.HighlightingManager.Instance.GetDefinition("BAT");
                    return true;
                case "lang csharp":
                    textEditor.SyntaxHighlighting = ICSharpCode.AvalonEdit.Highlighting.HighlightingManager.Instance.GetDefinition("CSharp");
                    return true;
                case "lang processing":
                    textEditor.SyntaxHighlighting = LangSelection.GetHighlightingDefinition("Processing");
                    return true;
                case "lang markdown":
                    textEditor.SyntaxHighlighting = ICSharpCode.AvalonEdit.Highlighting.HighlightingManager.Instance.GetDefinition("Markdown");
                    return true;
                default:
                    return false;
            }
        }
        public static Task<T> StartSTATask<T>(Func<T> func)
        {
            var tcs = new TaskCompletionSource<T>();
            Thread thread = new Thread(() =>
            {
                try
                {
                    tcs.SetResult(func());
                }
                catch (Exception e)
                {
                    tcs.SetException(e);
                }
            });
            thread.SetApartmentState(ApartmentState.STA);
            thread.Start();
            return tcs.Task;
        }
        private void OutputWriteLine(string format, params object[] args)
        {
            outputBox.Text += string.Format(format, args)+"\r\n";
        }

    }
}
