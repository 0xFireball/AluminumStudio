﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.IO;
using System.Reflection;
using Newtonsoft.Json;
using AluminumStudio.Subwindows.Objects;
using AluminumStudio.Subwindows.ToolWindows;

namespace AluminumStudio.Subwindows
{
    /// <summary>
    /// Interaction logic for CreateNewProjectDialog.xaml
    /// </summary>
    public partial class CreateNewProjectDialog
    {
        public string outputProjectFile=null;
        static char dirSep = System.IO.Path.DirectorySeparatorChar;
        static string appDir = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + dirSep;
        string templateDir = appDir + "Templates" + dirSep;
        private string templateManifestFileName = "template.json";

        public CreateNewProjectDialog()
        {
            InitializeComponent();
            UpdateMenus();
        }

        private void UpdateMenus()
        {
            projectLocationTb.Text = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + dirSep + "AluminumStudio" + dirSep;
            DiscoverTemplates();
        }

        private void DiscoverTemplates()
        {
            if (!Directory.Exists(templateDir))
                Directory.CreateDirectory(templateDir);
            DirectoryInfo[] templates = new DirectoryInfo(templateDir).GetDirectories();
            foreach (var template in templates)
            {
                try
                {
                    string folLoc = template.FullName + dirSep;
                    string templateManifestJson = File.ReadAllText(folLoc + templateManifestFileName);
                    var templateManifest = JsonConvert.DeserializeObject<ProjectTemplate>(templateManifestJson);
                    templateManifest.FolderLocation = folLoc;
                    templateList.Items.Add(templateManifest);
                }
                catch (FileNotFoundException)
                {
                    MessageBox.Show("An invalid template was found. Please verify your template installations.");
                }
            }
        }

        private void projectNameTb_TextChanged(object sender, TextChangedEventArgs e)
        {
            UpdateFinalOutput();
        }

        private void projectLocationTb_TextChanged(object sender, TextChangedEventArgs e)
        {
            UpdateFinalOutput();
        }

        private void UpdateFinalOutput()
        {
            if (projectNameTb != null && projectNameTb != null && finalOutputLoc != null)
                finalOutputLoc.Text = projectLocationTb.Text + projectNameTb.Text;
        }

        private void templateList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            createProjectBtn.IsEnabled = templateList.SelectedItem != null;
            if (templateList.SelectedItem!=null)
            {
                projectNameTb.Text = ((ProjectTemplate)templateList.SelectedItem).Name.Replace(" ","")+"1";
            }
        }

        private void createProjectBtn_Click(object sender, RoutedEventArgs e)
        {
            CopyTemplateAndCreateProject();
        }

        private void CopyTemplateAndCreateProject()
        {
            try
            {
                var cpStatus = new creatingProjectToolWindow();
                cpStatus.Show();
                string templateDir = ((ProjectTemplate)templateList.SelectedItem).FolderLocation;
                string projectDir = finalOutputLoc.Text+dirSep;
                if (!Directory.Exists(projectDir))
                    Directory.CreateDirectory(projectDir);
                else
                {
                    cpStatus.Close();
                    MessageBox.Show("The project directory is not empty.");
                    return;
                }
                CopyDirectory(templateDir, projectDir);
                File.Delete(projectDir + templateManifestFileName);
                outputProjectFile = projectDir + projectNameTb.Text + ".aluminumproj";
                File.Move(projectDir + "project.aluminumproj", outputProjectFile);
                cpStatus.Close();
                this.Close();
            }
            catch (IOException iox)
            {
                this.Show();
                MessageBox.Show("An error occurred creating the project: " + iox.ToString());
            }
        }

        static void CopyDirectory(string SourcePath, string DestinationPath)
        {
            //Now Create all of the directories
            foreach (string dirPath in Directory.GetDirectories(SourcePath, "*",
                SearchOption.AllDirectories))
                Directory.CreateDirectory(dirPath.Replace(SourcePath, DestinationPath));

            //Copy all the files & Replaces any files with the same name
            foreach (string newPath in Directory.GetFiles(SourcePath, "*.*",
                SearchOption.AllDirectories))
                File.Copy(newPath, newPath.Replace(SourcePath, DestinationPath), true);
        }

        private void browseBtn_Click(object sender, RoutedEventArgs e)
        {
            var ofd = new System.Windows.Forms.FolderBrowserDialog();
            ofd.Description = "Select Project";
            if (ofd.ShowDialog()==System.Windows.Forms.DialogResult.OK)
            {
                projectLocationTb.Text = ofd.SelectedPath;
            }
        }
    }
}
