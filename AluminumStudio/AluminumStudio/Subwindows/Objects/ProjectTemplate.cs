﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AluminumStudio.Subwindows.Objects
{
    class ProjectTemplate
    {
        public string Name;
        public string Version;
        public string FolderLocation;

        public ProjectTemplate(string name, string version)
        {
            this.Name = name;
            this.Version = version;
        }

        public override string ToString()
        {
            return Name;
        }
    }
}
