﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using AluminumStudio.Subwindows;
using AluminumStudio.CoreWindows;
using Microsoft.Win32;

namespace AluminumStudio
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void createProjectBtn_Click(object sender, RoutedEventArgs e)
        {
            CreateProject();
        }

        private void CreateProject()
        {
            this.Hide();
            CreateNewProjectDialog newProjectDlg = new CreateNewProjectDialog();
            newProjectDlg.ShowDialog();
            if (newProjectDlg.outputProjectFile != null)
            {
                LaunchEditor(newProjectDlg.outputProjectFile);
            }
            else
            {
                this.Show();
            }
        }

        private void LaunchEditor(string outputProjectFile)
        {
            var editor = new IntegratedEditor1(outputProjectFile);
            editor.ShowDialog();
            this.Show();
        }

        private void openProjectBtn_Click(object sender, RoutedEventArgs e)
        {
            OpenProject();
        }

        private void OpenProject()
        {
            var ofd = new OpenFileDialog();
            ofd.Title = "Select Project";
            ofd.Filter = "Aluminum Studio Projects (.aluminumproj)|*.aluminumproj";
            ofd.Multiselect = false;
            if ((bool)ofd.ShowDialog())
            {
                var editor = new IntegratedEditor1(ofd.FileName);
                try
                {
                    this.Hide();
                    editor.ShowDialog();
                }
                catch (System.Reflection.TargetInvocationException tex)
                {
                    editor.Close();
                    new ErrorDisplay(tex.ToString(), CrashType.WholeStudio).ShowDialog();
                }
                catch (Exception ex)
                {
                    editor.Close();
                    new ErrorDisplay(ex.ToString(), CrashType.WholeStudio).ShowDialog();
                }
                this.Show();
            }
        }

        private void alStdLabel_MouseDown(object sender, MouseButtonEventArgs e)
        {
            new About().ShowDialog();
        }

        private void textBlock_MouseDown(object sender, MouseButtonEventArgs e)
        {
            new About().ShowDialog();
        }
    }
}
