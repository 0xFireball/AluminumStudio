﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.IO;

namespace AluminumStudio.StudioInternal.ProjectSupport.LaunchWindows
{
    /// <summary>
    /// Interaction logic for ProcessingWebLaunchWindow.xaml
    /// </summary>
    public partial class ProcessingWebLaunchWindow : Window
    {
        string __pjsCode;
        DirectoryInfo _outputDir;
        int act;

        public ProcessingWebLaunchWindow(string processingCode, DirectoryInfo outputDir, int action)
        {
            act = action;
            //ACTION 1 = build
            _outputDir = outputDir;
            __pjsCode = processingCode;
            InitializeComponent();
            this.Opacity = 0;
            this.ShowInTaskbar = false;
            this.WindowState = WindowState.Minimized;
            this.Visibility = Visibility.Hidden;
            wbBrowser.Loaded += (s, e) => { LoadPage(); };
        }

        private void LoadPage()
        {
            string docH = Properties.Resources.weblauncher.Replace("[PROCESSING_CODE_TARGET]", __pjsCode).Replace("[PJSSCRIPT]", Properties.Resources.processing);
            File.WriteAllText(_outputDir.FullName+Path.DirectorySeparatorChar+"index.html", docH);
            if (act == 0)
            {
                this.WindowState = WindowState.Normal;
                this.Visibility = Visibility.Visible;
                this.Opacity = 1.0;
                this.ShowInTaskbar = true;
                wbBrowser.NavigateToString(docH);
            }
            else
                this.Close();
        }
    }
}
