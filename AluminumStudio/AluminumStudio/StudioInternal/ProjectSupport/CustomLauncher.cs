﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AluminumStudio;

namespace AluminumStudio.StudioInternal.ProjectSupport
{
    class CustomLauncher : IAluminumProjectLauncher
    {
        public bool _canLaunch = false;
        public bool _canBuild = false;
        public bool CanLaunch
        {
            get
            {
                return _canLaunch;
            }
            set
            {
                _canLaunch = value;
            }
        }
        public bool CanBuild
        {
            get
            {
                return _canBuild;
            }
            set
            {
                _canBuild = value;
            }
        }
        public bool TerminateRunningValid
        {
            get
            {
                return _terminateRunningValid;
            }
        }
        public bool IsRunning
        {
            get
            {
                return _isRunning;
            }
        }
        bool _isRunning;
        public string LaunchItem=null;
        public string[] LaunchArguments=null;
        public string BuildItem = null;
        public string[] BuildArguments = null;
        bool _terminateRunningValid = false;
        public void Launch(params object[] args)
        {
            _isRunning = true;
            //launch
            try
            {
                string pcwd = Environment.CurrentDirectory;
                Environment.CurrentDirectory = (string)args[0];
                var startArgs = string.Join(" ", LaunchArguments);
                System.Diagnostics.Process.Start(LaunchItem, startArgs);
                Environment.CurrentDirectory = pcwd;
            }
            catch (Exception ex)
            {
                new ErrorDisplay(ex.ToString(), CrashType.ActionCrash).ShowDialog();
            }
            _isRunning = false;
        }
        public void Build(params object[] args)
        {
            _isRunning = true;
            //launch
            try
            {
                string pcwd = Environment.CurrentDirectory;
                Environment.CurrentDirectory = (string)args[0];
                var startArgs = string.Join(" ", BuildArguments);
                System.Diagnostics.Process.Start(BuildItem, startArgs);
                Environment.CurrentDirectory = pcwd;
            }
            catch (Exception ex)
            {
                new ErrorDisplay(ex.ToString(), CrashType.ActionCrash).ShowDialog();
            }
            _isRunning = false;
        }
        public void TerminateRunning(params object[] args)
        {
            if (_isRunning && _terminateRunningValid)
            {
                //Terminate
            }
        }
    }
}
