﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AluminumStudio.StudioInternal.ProjectSupport.LaunchWindows;

namespace AluminumStudio.StudioInternal.ProjectSupport
{
    class ProcessingWebLauncher : IAluminumProjectLauncher
    {
        public bool _canLaunch = true;
        public bool _canBuild = true;
        public bool CanLaunch
        {
            get
            {
                return _canLaunch;
            }
        }
        public bool CanBuild
        {
            get
            {
                return _canBuild;
            }
        }
        List<ProcessingWebLaunchWindow> launchWins = new List<ProcessingWebLaunchWindow>();
        public bool TerminateRunningValid
        {
            get
            {
                return _terminateRunningValid;
            }
        }
        public bool IsRunning
        {
            get
            {
                return _isRunning;
            }
        }
        bool _isRunning;
        bool _terminateRunningValid=false;
        public void Launch(params object[] args)
        {
            if (_isRunning)
            {
                foreach (var win in launchWins)
                {
                    win.Dispatcher.Invoke(() =>
                    {
                        win.Close();
                    });
                }
            }
            _isRunning = true;
            //launch
            var lw = new ProcessingWebLaunchWindow((string)args[0],(System.IO.DirectoryInfo)args[1], 0);
            launchWins.Add(lw);
            lw.Closed += (s, e) => { _isRunning = false; };
            lw.ShowDialog();
        }
        public void Build(params object[] args)
        {
            if (_isRunning)
            {
                foreach (var win in launchWins)
                {
                    win.Dispatcher.Invoke(() =>
                    {
                        win.Close();
                    });
                }
            }
            _isRunning = true;
            //launch
            var lw = new ProcessingWebLaunchWindow((string)args[0], (System.IO.DirectoryInfo)args[1], 1);
            launchWins.Add(lw);
            lw.Closed += (s, e) => { _isRunning = false; };
            lw.ShowDialog();
        }
        public void TerminateRunning(params object[] args)
        {
            if (_isRunning && _terminateRunningValid)
            {
                //Terminate
            }
        }
    }
}
