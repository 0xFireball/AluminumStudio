﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AluminumStudio.StudioInternal.ProjectSupport
{
    interface IAluminumProjectLauncher
    {
        void Launch(params object[] args);
        void Build(params object[] args);
        void TerminateRunning(params object[] args);
        bool IsRunning { get; }
        bool CanLaunch { get; }
        bool CanBuild { get; }
    }
}
