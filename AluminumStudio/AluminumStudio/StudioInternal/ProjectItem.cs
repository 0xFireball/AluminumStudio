﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace AluminumStudio.StudioInternal
{
    class ProjectItem
    {
        public string DisplayName;
        public string FullPath;
        public ProjectItem(string fullPath)
        {
            this.FullPath = fullPath;
            this.DisplayName = Path.GetFileName(fullPath);
        }
        public override string ToString()
        {
            return DisplayName;
        }
    }
}
