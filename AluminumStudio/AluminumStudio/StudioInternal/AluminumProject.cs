﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AluminumStudio.StudioInternal
{
    public enum AluminumProjectType
    {
        Generic,
        TextProject,
        ProcessingWeb,
    }
    class AluminumProject
    {
        public string Name = null;
        public List<string> ProjectItems = null;
        public AluminumProjectType ProjectType = AluminumProjectType.Generic;
    }
}
