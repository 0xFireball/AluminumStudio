﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ICSharpCode.AvalonEdit.Highlighting;
using System.Xml;
using System.IO;

namespace AluminumStudio.StudioInternal
{
    static class LangSelection
    {
        private static Dictionary<string, IHighlightingDefinition> fileExtensionLanguageDefinitions = new Dictionary<string, IHighlightingDefinition>
        {
            {".pde",LoadHighlightingDefinition("Processing")},
            {".hx",LoadHighlightingDefinition("Haxe")},
        };

        public static IHighlightingDefinition GetHighlightingDefinition(string e)
        {
            if (fileExtensionLanguageDefinitions.ContainsKey(e))
                return fileExtensionLanguageDefinitions[e];
            else
                return LoadHighlightingDefinition(e);
        }

        private static IHighlightingDefinition LoadHighlightingDefinition(string v)
        {
            switch (v)
            {
                case "Processing":
                    using (var ms = new MemoryStream(Properties.Resources.processinghighlighting))
                    {
                        using (var sr = new StreamReader(ms))
                        {
                            using (XmlTextReader reader = new XmlTextReader(sr))
                            {
                                return ICSharpCode.AvalonEdit.Highlighting.Xshd.HighlightingLoader.Load(reader, ICSharpCode.AvalonEdit.Highlighting.HighlightingManager.Instance);
                            }
                        }
                    }
                case "Haxe":
                    using (var ms = new MemoryStream(Properties.Resources.haxehighlighting))
                    {
                        using (var sr = new StreamReader(ms))
                        {
                            using (XmlTextReader reader = new XmlTextReader(sr))
                            {
                                return ICSharpCode.AvalonEdit.Highlighting.Xshd.HighlightingLoader.Load(reader, ICSharpCode.AvalonEdit.Highlighting.HighlightingManager.Instance);
                            }
                        }
                    }
                default:
                    return null;
            }
        }
    }
}
